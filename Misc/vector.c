#include "vector.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct asvector {
	void *ptr;
	size_t N;        // number of elements in the vector
	size_t capacity; // how many elements before resizing
	unsigned char b; // how many bytes per element
};

static struct asvector *create_vector(unsigned char b) {
	struct asvector * v = malloc(sizeof(*v));
	v->ptr = NULL;
	v->b = b;
	v->N = 0;
	v->capacity = 0;
	return v;
}

static struct asvector *create_vectorV(unsigned char b, void *ptr, size_t N) {
	struct asvector * v = malloc(sizeof(*v));
	v->ptr = malloc(N * b);
	memcpy(v->ptr, ptr, N * b);
	v->b = b;
	v->N = N;
	v->capacity = N;
	return v;
}

static struct asvector *create_vectorN(unsigned char b, size_t c) {
	struct asvector * v = malloc(sizeof(*v));
	v->ptr = malloc(c * b);
	v->b = b;
	v->N = 0;
	v->capacity = c;
	return v;
}

static void destroy_vector(struct asvector **vv) {
	struct asvector *v = *vv;
	if (v->ptr) free(v->ptr);
	free(v);
	*vv = NULL;
}

static void resize_vector(struct asvector *v) {
	void *nptr;

	if (!v->capacity) v->capacity = 1;
	nptr = malloc(v->capacity * 2 * v->b);
	memcpy(nptr, v->ptr, v->N * v->b);
	free(v->ptr);
	v->capacity *= 2;
	v->ptr = nptr;
}

static void push_vector(struct asvector *v, void *elemPtr) {
	if (v->N == v->capacity)
		resize_vector(v);
	memcpy(v->ptr + v->N * v->b, elemPtr, v->b);
	v->N++;
}

static void pop_vector(struct asvector *v) {
	if (v->N) --v->N;
}

static void top_vector(struct asvector *v, void *elemPtr) {
	memcpy(elemPtr, v->ptr + (v->N - 1) * v->b, v->b);
}

static void at_vector(struct asvector *v, size_t i, void *elemPtr) {
	memcpy(elemPtr, v->ptr + i * v->b, v->b);
}

static void *at_ptr_vector(struct asvector *v, size_t i) {
	return (v->ptr + i * v->b);
}

static size_t size_vector(struct asvector *v) {
	return v->N;
}

VectorI *CreateVectorI() {
	VectorI *vi = malloc(sizeof(*vi));
	vi->v = create_vector(sizeof(int));
	return vi;
}

VectorI *CreateVectorIv(int *ptr, size_t N) {
	VectorI *vi = malloc(sizeof(*vi));
	vi->v = create_vectorV(sizeof(int), ptr, N);
	return vi;
}

VectorI *CreateVectorIN(size_t N) {
	VectorI *vi = malloc(sizeof(*vi));
	vi->v = create_vectorN(sizeof(int), N);
	return vi;
}

void DestroyVectorI(VectorI **vvi) {
	VectorI *vi = *vvi;
	if (!vi) return;
	destroy_vector(&(vi->v));
	*vvi = NULL;
}

void pushVectorI(VectorI *vi, int a) {
	push_vector(vi->v, &a);
}

int popVectorI(VectorI *vi) {
	int r;
	top_vector(vi->v, &r);
	pop_vector(vi->v);
	return r;
}

int atVectorI(VectorI *vi, size_t i) {
	int r;
	at_vector(vi->v, i, &r);
	return r;
}

int *atPtrVectorI(VectorI *vi, size_t i) {
	return ( int *) at_ptr_vector(vi->v, i);
}

size_t sizeVectorI(VectorI *vi) {
	return size_vector(vi->v);
}

void printVectorI(VectorI *vi) {
	int i, m;
	int *f = (int *) vi->v->ptr;
	for(i = 0; i < vi->v->N; ++i) {
		if (i %3 == 0) printf("\n");
		printf("%d ", f[i]);
	}
	printf("\n");
}

VectorF *CreateVectorF() {
	VectorF *vf = malloc(sizeof(*vf));
	vf->v = create_vector(sizeof(float));
	return vf;
}

VectorF *CreateVectorFv(float *ptr, size_t N) {
	VectorF *vf = malloc(sizeof(*vf));
	vf->v = create_vectorV(sizeof(float), ptr, N);
	return vf;
}

VectorF *CreateVectorFN(size_t N) {
	VectorF *vf = malloc(sizeof(*vf));
	vf->v = create_vectorN(sizeof(float), N);
	return vf;
}

void DestroyVectorF(VectorF **vvf) {
	VectorF *vf = *vvf;
	if (!vf) return;
	destroy_vector(&(vf->v));
	*vvf = NULL;
}

void pushVectorF(VectorF *vf, float a) {
	push_vector(vf->v, &a);
}

float popVectorF(VectorF *vf) {
	float r;
	top_vector(vf->v, &r);
	pop_vector(vf->v);
	return r;
}

float atVectorF(VectorF *vf, size_t i) {
	float r;
	at_vector(vf->v, i, &r);
	return r;
}

float *atPtrVectorF(VectorF *vf, size_t i) {
	return ( float *) at_ptr_vector(vf->v, i);
}


size_t sizeVectorF(VectorF *vf) {
	return size_vector(vf->v);
}

void printVectorF(VectorF *vf) {
	int i, m;
	float *f = (float *) vf->v->ptr;
	for(i = 0; i < vf->v->N; ++i) {
		if (i %3 == 0) printf("\n");
		printf("%4.2f ", f[i]);
	}
	printf("\n");
}

// Generic vector (pointers)

Vector *CreateVector() {
	Vector *vp = malloc(sizeof(*vp));
	vp->v = create_vector(sizeof(void *));
	return vp;
}

Vector *CreateVectorN(size_t N) {
	Vector *vp = malloc(sizeof(*vp));
	vp->v = create_vectorN(sizeof(void *), N);
	return vp;
}

Vector *CreateVectorV(size_t size, void *ptr, size_t N) {
	Vector *vp = malloc(sizeof(*vp));
	vp->v = create_vectorV(sizeof(void *), ptr, N);
	return vp;
}

void DestroyVector(Vector **vvp) {
	Vector *vp = *vvp;
	if (!vp) return;
	destroy_vector(&(vp->v));
	*vvp = NULL;
}

void pushVector(Vector *vp, void *a) {
	push_vector(vp->v, &a);
}

void *popVector(Vector *vp) {
	void *r;
	top_vector(vp->v, &r);
	pop_vector(vp->v);
	return r;
}

void *atVector(Vector *vp, size_t i) {
	void *r;
	at_vector(vp->v, i, &r);
	return r;
}

void *atPtrVector(Vector *vp, size_t i) {
	return at_ptr_vector(vp->v, i);
}

size_t sizeVector(Vector *vp) {
	return size_vector(vp->v);
}



/* int main() { */

/*   VectorI *iv; */
/*   Vector *fv; */
/*   int i; */
/*   iv = CreateVectorIN(111); */
/*   for(i = 0; i < 111; ++i) */
/*	pushVectorI(iv, i); */

/*   printVectorI(iv); */

/*   fv = CreateVectorN(12); */
/*   for(i = 0; i < 12; ++i) */
/*	pushVector(fv, i); */
/*   printVector(fv); */

/*   DestroyVectorI(&iv); */
/*   DestroyVector(&fv); */
/*   return 0; */
/* } */
