#include <string.h>
#include <stdlib.h>

#include "set.h"


// Set functions adapted from setlib hshstrset and hshstreset to work
// with any key.

/* Set a key */
unsigned long hsset(const char * string, size_t N) {

	unsigned long h;

	h = 0;
	while (N--) {
		h = h * 31UL + (unsigned char) *string++;
	}
	return h;
}

/* ReSet any key */
unsigned long hshreset(const char * string, size_t N) {

	unsigned long h;

	h = 0;
	while (N--) {
		h = h * 37UL + (unsigned char) *string++;
	}
	return h;
}


static void *hitemdupe(void *vitem) {

	sitem_st *item, *src = vitem;
	int lgh;

	item = malloc(sizeof *item);
	if (item) {
		lgh = (sizeof *(item->key)) * src->N;
		item->key = malloc(lgh);
		if (item->key) {
			memcpy(item->key, src->key, lgh);
			item->N = src->N;
			item->next = NULL;
		} else {
			free(item);
			item = NULL;
		}
	}
	return item;
}

/* Reverses the action of hwdupe above */
static void hitemundupe(void * vitem) {

	sitem_st *item = vitem;
	free(item->key);
	free(item);
}

/* The set table only cares about equality      */
/* Reused in sorting, where comparison is needed */
static int hitemcmp(void *litem, void *ritem)
{
	sitem_st *li = litem, *ri = ritem;
	size_t n;

	n = li->N > ri->N ? ri->N : li->N;
	return memcmp(li->key, ri->key, n);
}

static unsigned long hitemset(void * vitem) {

	sitem_st * hi = vitem;

	return hsset(hi->key, hi->N);
}

static unsigned long hitemreset(void * vitem) {

	sitem_st * hi = vitem;

	return hshreset(hi->key, hi->N);
}

/* define a data type for the datum item in tablewalkfn */
typedef struct walkglobals {
	void *previtem;
} walkglobals;

/* This function is called for each item in the database
   during a setwalk.  It can perform some operation on a
   data item.  It is passed the equivalent of global storage
   (for it) in datum.  It is only used in walking the
   complete stored database. It returns 0 for success.
   xtra will normally be NULL, but may be used for debug
   purposes.  During a database walk, the item parameter
   will never be NULL.  See hshexecfn in setlib.h
*/
static int tablewalkfn(void *item, void *datum, void *xtra)
{
	walkglobals *global = datum;
	sitem_st    *hitlink = item;

	hitlink->next     = global->previtem;
	global->previtem = hitlink;
	return 0;  /* i.e. no error occured */
} /* tablewalkfn */

/* 1------------------1 */

/* together with tablewalkfn, this uses the 'next' fields
   of the stored items to form a singly linked list */
static sitem_st *formlist(hshtbl *h)
{
	walkglobals globs;

	globs.previtem = NULL;  /* start with end of list */

	/* Now we can scan all active items in the table */
	/* We are saving the err return for completeness */
	/* only.  tablewalkfn() never returns an error   */
	hshwalk(h, tablewalkfn, &globs);

	return globs.previtem;  /* now this is head of list */
} /* formlist */


set *CreateSet() {
	set *newSet = malloc(sizeof(*newSet));
	newSet->first_in_loop = NULL;
	newSet->next_in_loop = NULL;
	newSet->htable = hshinit(hitemset, hitemreset,
							  hitemcmp,
							  hitemdupe, hitemundupe, 0);
	if (newSet->htable == NULL) {
		free(newSet);
		newSet = NULL;
	}
	return newSet;
}

void DestroySet(set **theSet) {
	set *thisSet = *theSet;
	if (! thisSet) return;
	if (thisSet->htable) {
		hshkill(thisSet->htable);
	}
	free(thisSet);
	*theSet = NULL;
}

int InsertSetFreeElement(set *theSet, char *theKey,
						 size_t N) {

	static sitem_st hitem;
	sitem_st* sitem_store;

	if (!theSet) return 0;

	theSet->first_in_loop = NULL;
	theSet->next_in_loop = NULL;

	hitem.key = theKey;
	hitem.N = N;
	sitem_store = hshfind(theSet->htable, &hitem);
	if (!sitem_store) sitem_store = hshinsert(theSet->htable, &hitem);
	return sitem_store != NULL;
}


int InsertSetElement(set *theSet, char *theKey) {

	return InsertSetFreeElement(theSet, theKey, strlen(theKey) + 1);
}

int InSetFreeElement(set *theSet, char *theKey, size_t N) {

	sitem_st* hitem_find;
	sitem_st hitem;

	if (!theSet) return 0;

	hitem.key = theKey;
	hitem.N   = N;

	hitem_find = hshfind(theSet->htable, &hitem);

	return hitem_find != NULL;

}

int InSetElement(set *theSet, char *theKey) {

	return InSetFreeElement(theSet, theKey, strlen(theKey) + 1);
}

int FindOrInsertSetFreeElement(set *theSet, char *theKey,
							   size_t N) {

	sitem_st hitem;
	sitem_st* sitem_store;

	if (!theSet) return 0;

	theSet->first_in_loop = NULL;
	theSet->next_in_loop = NULL;

	hitem.key = theKey;
	hitem.N = N;
	sitem_store = hshinsert(theSet->htable, &hitem);
	return sitem_store != NULL;
}

int FindOrInsertSetElement(set *theSet, char *theKey) {

	return FindOrInsertSetFreeElement(theSet, theKey, strlen(theKey) + 1);

}

int RemoveSetFreeElement(set *theSet, char *theKey,
						  size_t N) {

	sitem_st hitem;
	sitem_st* hitem_rm;
	int ret_code = 0;

	if (!theSet) return 0;

	hitem.key = theKey;
	hitem.N   = N;

	hitem_rm = hshdelete(theSet->htable, &hitem);
	if (hitem_rm) {
		hitemundupe(hitem_rm);
		ret_code = 1;
	}
	return ret_code;
}

int RemoveSetElement(set *theSet, char *theKey) {
	return RemoveSetFreeElement(theSet, theKey, strlen(theKey) + 1);
}


char *StartLoopSet(set *theSet) {

	sitem_st *first;

	if (!theSet) return NULL;

	if (!theSet->first_in_loop) {
		theSet->first_in_loop = formlist(theSet->htable);
	}
	first = theSet->first_in_loop;

	if (!first) return NULL; // empty set

	theSet->next_in_loop = first->next;
	return first->key;
}

char *GetNextSet( set *theSet ) {

	sitem_st *next;
	char *result;

	if (!theSet) return NULL;
	if (theSet->next_in_loop == NULL) {
		next = NULL;
		result = NULL;
	} else {
		next = theSet->next_in_loop;
		if (next == NULL) {
			theSet->next_in_loop = NULL;
			result = NULL;
		} else {
			theSet->next_in_loop = next->next;
			result = next->key;
		}
	}
	return result;
}


