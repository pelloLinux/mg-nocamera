#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "list.h"
#include "hash.h"
#include "set.h"
#include "vector.h"
#include "parse_scene_json.h"
#include "trfm3D.h"
#include "scene.h"
#include "node.h"
#include "gObjectManager.h"
#include "shaderManager.h"
#include "lightManager.h"
#include "cameraManager.h"
#include "avatarManager.h"

static void ps_error(char *f, char *str) {


	fprintf(stderr, "[E] %s: %s\n", f, str);
	exit(1);

}

static void print_vector(Vector *v) {

	size_t i;
	size_t n = sizeVector(v);
	if (!n) return;
	n--;
	printf("[");
	for( i = 0; i < n; ++i) {
		printf("%s,", (char *) atVector(v, i));
	}
	printf("%s]", (char *) atVector(v, n));
}

static void print_jhash(hash *h) {

	char *k;
	hash *hh;

	hh = FindHashElement(FindHashElement(h, "Scene"), "node");

	k = StartKeyLoopHash(hh);
	for(; k; printf ("\n"), k = GetNextKeyHash(hh)) {
		printf ("%s: ", k);
		if (!strcmp(k, "child")) continue;
		if (!strcmp(k, "v")) {
			print_vector((Vector *) FindHashElement(hh, k));
			continue;
		}
		//printf ("%s ", (char *) FindHashElement(hh, k));
	}

}

// Given a parsed json (a hash), populate the scene

// Destroy vector and content (char *)

static void wipe_vector(Vector *v) {
	size_t i, n;
	char *str;
	n = sizeVector(v);
	for(i = 0; i < n; ++i) {
		str = atVector(v, i);
		free(str);
	}
	DestroyVector(&v);
}

// destroy a hash
// print warnigs if hash is not empty

static void check_and_destroy(char *str, hash *h, set *S) {
	static char buf[4096];
	char *k;
	int i = 0;
	buf[i] = 0;
	k = StartKeyLoopHash(h);
	while(k) {
		if (!InSetElement(S, k)) {
			strcat(&buf[i], k);
			i += strlen(k);
			strcat(&buf[i], ",");
			i++;
		}
		k = GetNextKeyHash(h);
	}
	if (strlen(buf)) {
		fprintf(stderr, "[W] %s: not empty hash (%s)\n", str, buf);
	}
	DestroyHash(&h);
	DestroySet(&S);
}

static void read_xyz(Vector *v, float *x, float *y, float *z) {

	char *s;
	s = (char *) atVector(v, 0);
	sscanf(s, "%f", x);
	s = (char *) atVector(v, 1);
	sscanf(s, "%f", y);
	s = (char *) atVector(v, 2);
	sscanf(s, "%f", z);
	if (v) wipe_vector(v);
}

static void read_fv(Vector *v, size_t n, float *V) {

	size_t i = 0;
	char *s;
	for(i = 0; i < n; i++) {
		s = (char *) atVector(v, i);
		sscanf(s, "%f", V + i);
	}
	if (v) wipe_vector(v);
}


// return 0 on success, 1 on error
static int read_hash_f(hash *h, set *S, char *key, float *f) {

	char *aux;
	aux = (char *) FindHashElement(h, key);
	int res = 1;
	if (aux) {
		InsertSetElement(S, key);
		aux = (char *) FindHashElement(h, key);
		sscanf(aux, "%f", f);
		free(aux);
		res = 0;
	}
	return res;
}

static int populate_gobjs(Vector *gObjects) {

	size_t i;
	size_t n;
	hash *gobj_hash;
	set *S;
	char *fname, *dirname;

	if (!gObjects) return -1;
	n = sizeVector(gObjects);
	for(i = 0; i < n; i++) {
		S = CreateSet();
		gobj_hash = (hash *) atVector(gObjects, i);
		fname = (char *) FindHashElement(gobj_hash, "fname");
		dirname= (char *) FindHashElement(gobj_hash, "dirname");
		if (!fname) return i;
		if (!dirname) return i;
		InsertSetElement(S, "fname");
		InsertSetElement(S, "dirname");
		SceneRegisterGObject(dirname, fname);
		// clean
		free(fname);
		free(dirname);
		check_and_destroy("populate_gobjs", gobj_hash, S);
	}
	// clean
	DestroyVector(&gObjects);
	return -1;
}

static int populate_shaders(Vector *shaders) {

	size_t i;
	size_t n;
	hash *shader;
	set *S;
	char *name, *vshader, *fshader;

	if (!shaders) return -1;
	n = sizeVector(shaders);
	for(i = 0; i < n; i++) {
		S = CreateSet();
		shader = (hash *) atVector(shaders, i);
		name = (char *) FindHashElement(shader, "name");
		vshader = (char *) FindHashElement(shader, "vshader");
		fshader = (char *) FindHashElement(shader, "fshader");
		if (!name) return i;
		if (!vshader) return i;
		if (!fshader) return i;
		InsertSetElement(S, "name");
		InsertSetElement(S, "vshader");
		InsertSetElement(S, "fshader");
		SceneRegisterShader(name, vshader, fshader);
		// clean
		free(name);
		free(vshader);
		free(fshader);
		check_and_destroy("populate_shaders", shader, S);
	}
	DestroyVector(&shaders);
	return -1;
}

static int populate_avatars(Vector *avatars) {

	size_t i;
	size_t n;
	hash *avatar;
	char *name, *camera;
	char *aux;
	set *S;
	float radius;
	int err = 0;
	Avatar *newAvatar;
	Camera *theCamera;

	if(!avatars) return -1;
	n = sizeVector(avatars);
	for(i = 0; i < n; i++) {
		S = CreateSet();
		err = 0;
		radius = 1.0;
		avatar = (hash *) atVector(avatars, i);
		name = (char *) FindHashElement(avatar, "name");
		camera = (char *) FindHashElement(avatar, "camera");
		theCamera = SceneFindCamera(camera);
		err += theCamera == NULL;
		if (!name) return i;
		if (!camera) return i;
		InsertSetElement(S, "name");
		InsertSetElement(S, "camera");
		err += read_hash_f(avatar, S, "radius", &radius);
		if (radius <= 0.0) err++;
		if (!err) {
			newAvatar = SceneRegisterAvatar(name, theCamera, radius);
			if(!newAvatar) {
				err++;
			}
		}
		// clean
		free(name);
		free(camera);
		check_and_destroy("populate_avatars", avatar, S);
	}
	return -1;
}

static int populate_cameras(Vector *cameras) {

	size_t i;
	size_t n;
	hash *camera;
	char *name, *type;
	char *aux;
	set *S;
	Vector *vpos, *vlat, *vup;
	float fovy, aspect, near, far;
	float pos[3];
	float lookAt[3];
	float up[3];
	int err = 0;
	Camera *newCamera;

	if(!cameras) return -1;
	n = sizeVector(cameras);
	for(i = 0; i < n; i++) {
		S = CreateSet();
		vpos = vlat = vup = NULL;
		err = 0;
		camera = (hash *) atVector(cameras, i);
		name = (char *) FindHashElement(camera, "name");
		type = (char *) FindHashElement(camera, "type");
		if (!name) return i;
		if (!type) return i;
		InsertSetElement(S, "name");
		InsertSetElement(S, "type");
		err += read_hash_f(camera, S, "fovy", &fovy);
		err += read_hash_f(camera, S, "aspect", &aspect);
		err += read_hash_f(camera, S, "near", &near);
		err += read_hash_f(camera, S, "far", &far);
		if (fovy < 0.0) err++;
		if (aspect < 0.0) err++;
		if (near <= 0.0) err++;
		if (near <= 0.0 || far < near) err++;
		if (strcmp("perspective", type)) {
			err++;
			fprintf(stderr, "[W] populate_cameras: unknown type camera %s. Skipping.\n", name);
		}
		vpos = FindHashElement(camera, "pos");
		if (vpos) {
			InsertSetElement(S, "pos");
			read_xyz(vpos, &pos[0], &pos[1], &pos[2]);
		}
		vlat = FindHashElement(camera, "lookAt");
		if (vlat) {
			InsertSetElement(S, "lookAt");
			read_xyz(vlat, &lookAt[0], &lookAt[1], &lookAt[2]);
		}
		vup = FindHashElement(camera, "up");
		if (vup) {
			InsertSetElement(S, "up");
			read_xyz(vup, &up[0], &up[1], &up[2]);
		}
		if (!err) {
			newCamera = SceneRegisterCamera(name);
			if(newCamera) {
				InitConicCamera(newCamera, fovy, aspect, near, far);
				if (vpos && vlat && vup) {
					SetCamera(newCamera,
							  pos[0], pos[1], pos[2],
							  lookAt[0], lookAt[1], lookAt[2],
							  up[0], up[1], up[2]);
				}
			} else {
				err++;
			}
		}
		// clean
		free(name);
		free(type);
		check_and_destroy("populate_cameras", camera, S);
	}
	return -1;
}


static int check_light_type(char *type) {

	if (!strcmp(type, "positional")) return 1;
	if (!strcmp(type, "directional")) return 1;
	if (!strcmp(type, "spotlight")) return 1;
	return 0;
}

static int populate_lights(Vector *lights) {

	size_t i;
	size_t n;
	int s;
	hash *light_h;
	char *name, *type, *aux;
	Vector *v;
	set *S;

	float r, g, b, w;
	float rgb[3], pos4[4];
	float cutoff = 30.0;
	float exp = 10.0;

	Light *l;

	if (!lights) return -1;
	n  = sizeVector(lights);
	for(i = 0; i < n; i++) {
		S = CreateSet();
		light_h = (hash *) atVector(lights, i);
		name = (char *) FindHashElement(light_h, "name");
		type = (char *) FindHashElement(light_h, "type");
		if (!check_light_type(type)) {
			fprintf(stderr, "[E] populate_lights: bad light type %s (possible values \"positional\", \"directional\", \"spotlight\")\n", type);
			exit(1);
		}

		if (!name) return i;
		if (!type) return i;
		InsertSetElement(S, "name");
		InsertSetElement(S, "type");
		l = SceneRegisterLight(name);
		v = (Vector *) FindHashElement(light_h, "pos");
		if (v) {
			InsertSetElement(S, "pos");
			read_xyz(v, &pos4[0], &pos4[1], &pos4[2]);
			pos4[3] = 1.0;
			if (!strcmp(type, "directional")) pos4[3] = 0.0;
			SetPositionLight(l, &pos4[0]);
		}

		v = (Vector *) FindHashElement(light_h, "amb");
		if (v) {
			InsertSetElement(S, "amb");
			read_xyz(v, &rgb[0], &rgb[1], &rgb[2]);
			SetAmbientLight(l, &rgb[0]);
		}

		v = (Vector *) FindHashElement(light_h, "dif");
		if (v) {
			InsertSetElement(S, "dif");
			read_xyz(v, &rgb[0], &rgb[1], &rgb[2]);
			SetDiffuseLight(l, &rgb[0]);
		}

		v = (Vector *) FindHashElement(light_h, "spec");
		if (v) {
			InsertSetElement(S, "spec");
			read_xyz(v, &rgb[0], &rgb[1], &rgb[2]);
			SetSpecularLight(l, &rgb[0]);
		}

		if (!strcmp(type, "spotlight")) {
			aux = (char *) FindHashElement(light_h, "exp");
			if (aux) {
				InsertSetElement(S, "exp");
				sscanf(aux, "%f", &exp);
				if (exp < 0) {
					fprintf(stderr, "[E] populate_lights: negative exponent\n");
					exit(1);
				}
				free(aux);
			}
			aux = (char *) FindHashElement(light_h, "cutoff");
			if (aux) {
				InsertSetElement(S, "cutoff");
				sscanf(aux, "%f", &cutoff);
				if (exp < 0) {
					fprintf(stderr, "[E] populate_lights: negative cutoff\n");
					exit(1);
				}
				free(aux);
			}
			rgb[0] = 0.577; rgb[1] = 0.577; rgb[2] = 0.577;
			v = (Vector *) FindHashElement(light_h, "spdir");
			if (v) {
				InsertSetElement(S, "spdir");
				read_xyz(v, &rgb[0], &rgb[1], &rgb[2]);
			}
			SetSpotLight(l, &rgb[0], cutoff, exp);
		}
		aux = (char *) FindHashElement(light_h, "switched");
		if (aux) {
			InsertSetElement(S, "switched");
			sscanf(aux, "%d", &s);
			if (!s) SwitchLight(l, 0);
			free(aux);
		}
		free(type);
		free(name);
		check_and_destroy("populate_lights", light_h, S);
		//DestroyHash(&light_h);
	}
	DestroyVector(&lights);
	return -1;
}

// [ { "trans" : [0, -10, -100] }, { "rotX" : 30.0 }]

//
// possible keys
//
// trans : [ x, y, z ]
// rotX : angle
// rotY : angle
// rotZ : angle
// rotVec : [ vx, vy, vz, angle ]
// rotAxis : [ vx, vy, vz, Px, Py, Pz, angle ]

static void apply_trfm(hash *h, trfm3D *T) {

	trfm3D *res;
	Vector *v;
	float aux[7];
	set *S;

	S = CreateSet();
	v = FindHashElement(h, "trans");
	if (v) {
		InsertSetElement(S, "trans");
		read_xyz(v, &aux[0], &aux[1], &aux[2]);
		AddTransTrfm3D(T, aux[0], aux[1], aux[2]);
		goto atr_end;
	}

	v = FindHashElement(h, "rotVec");
	if (v) {
		InsertSetElement(S, "rotVec");
		read_fv(v, 4, &aux[0]);
		AddRotVecTrfm3D(T,
						aux[0], aux[1], aux[2],
						aux[3]);
		goto atr_end;
	}

	v = FindHashElement(h, "rotAxis");
	if (v) {
		InsertSetElement(S, "rotAxis");
		read_fv(v, 7, &aux[0]);
		AddRotAxisTrfm3D(T,
						 aux[0], aux[1], aux[2],
						 aux[3], aux[4], aux[5],
						 aux[6]);
		goto atr_end;
	}

	v = FindHashElement(h, "rotX");
	if (v) {
		InsertSetElement(S, "rotX");
		read_fv(v, 1, &aux[0]);
		AddRot_X_Trfm3D(T, aux[0]);
		goto atr_end;
	}

	v = FindHashElement(h, "rotY");
	if (v) {
		InsertSetElement(S, "rotY");
		read_fv(v, 1, &aux[0]);
		AddRot_Y_Trfm3D(T, aux[0]);
		goto atr_end;
	}

	v = FindHashElement(h, "rotZ");
	if (v) {
		InsertSetElement(S, "rotZ");
		read_fv(v, 1, &aux[0]);
		AddRot_Z_Trfm3D(T, aux[0]);
		goto atr_end;
	}

 atr_end:
	check_and_destroy("apply_trfm", h, S);
	return;
}

static trfm3D *parse_trfms(Vector *trfms) {

	size_t i;
	size_t n = sizeVector(trfms);
	hash *trfm;
	trfm3D *T;

	T = CreateTrfm3D();
	for(i = 0; i < n; ++i) {
		trfm = (hash *) atVector(trfms, i);
		apply_trfm(trfm, T);
	}
	DestroyVector(&trfms);
	return T;
}

static Node *populate_nodes(hash *nodeH) {

	Vector *v;
	trfm3D *T;
	size_t i, n;
	char *name, *str;
	int has_gobj = 0;
	GObject *gobj;
	set *S;

	Node *myNode;

	S = CreateSet();

	name = FindHashElement(nodeH, "name");
	if (name) InsertSetElement(S, "name");

	myNode = CreateNode(name);

	v = FindHashElement(nodeH, "trfm");
	if (v) {
		InsertSetElement(S, "trfm");
		T = parse_trfms(v);
		SetTrfmNode(myNode, T);
	}

	str = FindHashElement(nodeH, "shader");
	if (str) {
		InsertSetElement(S, "shader");
		SetShaderNode(myNode, SceneFindShader(str));
		free(str);
	}

	str = FindHashElement(nodeH, "gObj");
	if (str) {
		InsertSetElement(S, "gObj");
		has_gobj = 1;
		gobj = SceneFindNameGObject(str);
		if(!gobj) {
			fprintf(stderr, "[E] populate_node: %s not found\n", str);
			PrintRegisteredGobjs();
			exit(1);
		}
		SetGobjNode(myNode, gobj);
		free(str);
	}

	v = (Vector *) FindHashElement(nodeH, "childs");
	if (v) {
		InsertSetElement(S, "childs");
		if (has_gobj) {
			fprintf(stderr, "[E] populate_nodes: node %s has gObj and children\n", name);
			exit(1);
		}
		n = sizeVector(v);
		for(i = 0; i < n; ++i) {
			AttachNode(myNode, populate_nodes((hash *) atVector(v, i)));
		}
		DestroyVector(&v);
	}
	DestroyTrfm3D(&T);
	check_and_destroy("populate_nodes", nodeH, S);
	return myNode;
}


static Node * populate_scene(hash *scene_hash) {

	Node *root;
	if(!scene_hash) ps_error("populate_scene", "Empty scene");
	if (populate_cameras(FindHashElement(scene_hash, "cameras")) != -1) ps_error("populate_scene", "cameras");
	if (populate_avatars(FindHashElement(scene_hash, "avatars")) != -1) ps_error("populate_scene", "avatars");
	if (populate_gobjs(FindHashElement(scene_hash, "gObjects")) != -1) ps_error("populate_scene", "gObjects");
	if (populate_shaders(FindHashElement(scene_hash, "shaders")) != -1) ps_error("populate_scene", "shaders");
	if (populate_lights(FindHashElement(scene_hash, "lights")) != -1) ps_error("populate_scene", "lights");
	root = populate_nodes(FindHashElement(scene_hash, "node"));
	AttachNodeScene(root); // takes ownership
	return root;
}

Node *parse_scene(char *json_fname) {

	hash *h;

	h = parse_scene_json(json_fname);
	return populate_scene(FindHashElement(h, "Scene"));

}
