#ifndef PARSE_SCENE_H
#define PARSE_SCENE_H

#include "node.h"

Node *parse_scene(char *fname);

#endif
