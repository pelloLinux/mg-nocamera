
#include <stddef.h>
#include <stdio.h>
#include <math.h>
#include "projTexture.h"
#include "texture.h"
#include "textureManager.h"
#include "camera.h"
#include "tools.h"
#include "intersect.h"
#include "scene.h"
#include "trfm3D.h"

static void set_view_trfm( Camera *thisCamera);
static void UpdateFrustumPlanes(Camera * thisCamera);

void DestroyProjTexture(pText **thePTexture)
{
	pText * pText = *thePTexture;
	DestroyCamera(&pText->kamara);
	DestroyTrfm3D(&pText->T);

}

void BindGLProjTexture(pText * thePTexture)
{
	BindGLTextureTunit(thePTexture->texture, 2);

}
void PlaceSceneProjTexture(pText * thePTexture)
{
	RenderState      *rs;
	trfm3D           *modelView;

	if( ! thePTexture ) return;

	rs = RenderStateScene();
	modelView = TopRS(rs, MG_MODELVIEW);

	TransformPointTrfm3D(modelView, &thePTexture->kamara->Ex, &thePTexture->kamara->Ey,
						 &thePTexture->kamara->Ez);
	TransformPointTrfm3D(modelView, &thePTexture->kamara->AtX, &thePTexture->kamara->AtY, 
						&thePTexture->kamara->AtZ);
	TransformVectorTrfm3D(modelView, &thePTexture->kamara->UpX, &thePTexture->kamara->UpY,
					    &thePTexture->kamara->UpZ);


}


void CreateProjTexture(pText *thePTexture, char* image, float Ex, float Ey, float Ez,
						float AtX, float AtY, float AtZ,
						float UpX, float UpY, float UpZ,
						float fovy, float aspectRatio, float near, float far)
{


	//iruditik textura lortu
	thePTexture->texture = SceneRegisterProjTexture(image);
	
	Camera * kamara = CreateOriginCamera();

	//balioak estrukturan gorde
	kamara->Ex = Ex;
	kamara->Ey = Ey;
	kamara->Ez = Ez;

	kamara->AtX = AtX;
	kamara->AtY = AtY;
	kamara->AtZ = AtZ;

	kamara->UpX = UpX;
	kamara->UpY = UpY;
	kamara->UpZ = UpZ;

	kamara->fovy = fovy;
	kamara->aspectRatio = aspectRatio;
	kamara->near = near;
	kamara->far = far;

	thePTexture->kamara = kamara;


	trfm3D *Mb, *Mp, *Mts;//matrizeak erazagutu
	Mb = CreateTrfm3D();
	Mp = CreateTrfm3D();
	Mts = CreateTrfm3D();


	//*****************************************************
	//Proiekzio testuraren bista aldaketa matrizea kalkulatu
	//*****************************************************


	float Dx = Ex-AtX;// F = D
	float Dy = Ey-AtY;
	float Dz = Ez-AtZ;

	VectorNormalize(&Dx, &Dy, &Dz);
	VectorNormalize(&UpX, &UpY, &UpZ);

	float Rx;
	float Ry;
	float Rz;
	//Biderketa bektoriala
	crossVV(&Rx, &Ry, &Rz, UpX, UpY, UpZ, Dx, Dy, Dz);

	float Ux;
	float Uy;
	float Uz;
	//Biderketa bektoriala
	crossVV(&Ux, &Uy, &Uz, Dx, Dy, Dz, Rx, Ry, Rz);

	//Aldaketak kameran gorde
	kamara->Dx = Dx;
	kamara->Dy = Dy;
	kamara->Dz = Dz;

	kamara->Rx = Rx;
	kamara->Ry = Ry;
	kamara->Rz = Rz;

	kamara->Uy = Ux;
	kamara->Uy = Uy;
	kamara->Uz = Uz;
	
	set_view_trfm(kamara);


	//*****************************************************
	//Proiekzio matrizea kalkulatu
	//*****************************************************

	float t = near*tan(fovy/2);
	float b = (-1)*t;
	float r = aspectRatio*t;
	float l = (-1)*r;
	SetFrustumTrfm3D(Mp,l,r,t,b,near,far);

	//*****************************************************
	//Leku alaketa matrizea
	//*****************************************************

	SetTransTrfm3D(Mts,0.5,0.5,0.5);
	AddScaleTrfm3D(Mts,0.5);

	//Hiru matrizeak biderkatu
	trfm3D *lag = Mts;
	CompositionTrfm3D(lag,Mp);
	CompositionTrfm3D(lag,Mb);
	thePTexture->T = lag;

	//Shaderrari pasatzeko matrizea lortu
	GetGLMatrixTrfm3D(thePTexture->T, thePTexture->Tm);

}


static void set_view_trfm( Camera *thisCamera) {
	SetWorld2LocalTrfm3D(thisCamera->viewTrfm,
						 thisCamera->Ex, thisCamera->Ey, thisCamera->Ez,
						 thisCamera->Rx, thisCamera->Ry, thisCamera->Rz,
						 thisCamera->Ux, thisCamera->Uy, thisCamera->Uz,
						 thisCamera->Dx, thisCamera->Dy, thisCamera->Dz);

	UpdateFrustumPlanes(thisCamera);
}

static void UpdateFrustumPlanes(Camera * thisCamera) {

	static GLfloat M[16];
	static trfm3D T;
	plane *p;

	// V: view transformation
	// P: perspective transformation
	// T = P*V

	SetCopyTrfm3D(&T, thisCamera->projTrfm); // T = P;
	CompositionTrfm3D(&T, thisCamera->viewTrfm); // T = P*V
	GetGLMatrixTrfm3D(&T, M);

	// Extract the planes parameters

	// left plane
	p = thisCamera->fPlanes[0];

	p->nx = -M[3]  - M[0];  // -(m_41 + m_11)
	p->ny = -M[7]  - M[4];  // -(m_42 + m_12)
	p->nz = -M[11] - M[8];  // -(m_43 + m_13)
	p->d =   M[15] + M[12]; //  (m_44 + m_14), because d in plane is really (-d)
	p->is_norm = 0;
	p = thisCamera->fPlanes[1];

	// right plane

	p->nx = -M[3]  + M[0];  // -(m_41 - m_11)
	p->ny = -M[7]  + M[4];  // -(m_42 - m_12)
	p->nz = -M[11] + M[8];  // -(m_43 - m_13)
	p->d =   M[15] - M[12]; //  (m_44 - m_14) because d in plane is really (-d)
	p->is_norm = 0;
	p = thisCamera->fPlanes[2];

	// bottom plane

	p->nx = -M[3]  - M[1];  // -(m_41 + m_21)
	p->ny = -M[7]  - M[5];  // -(m_42 + m_22)
	p->nz = -M[11] - M[9];  // -(m_43 + m_23)
	p->d =   M[15] + M[13]; //  (m_44 + m_24) because d in plane is really (-d)
	p->is_norm = 0;
	p = thisCamera->fPlanes[3];

	// top plane

	p->nx = -M[3]  + M[1];   // -(m_41 - m_21)
	p->ny = -M[7]  + M[5];   // -(m_42 - m_22)
	p->nz = -M[11] + M[9];   // -(m_43 - m_23)
	p->d =   M[15] - M[13];  //  (m_44 - m_24) because d in plane is really (-d)
	p->is_norm = 0;
	p = thisCamera->fPlanes[4];

	// near plane

	p->nx = -M[3]  - M[2];  // -(m_41 + m_31)
	p->ny = -M[7]  - M[6];  // -(m_42 + m_32)
	p->nz = -M[11] - M[10]; // -(m_43 + m_33)
	p->d =   M[15] + M[14]; //  (m_44 + m_34) because d in plane is really (-d)
	p->is_norm = 0;
	p = thisCamera->fPlanes[5];

	// far plane

	p->nx = -M[3]  + M[2];  // -(m_41 - m_31)
	p->ny = -M[7]  + M[6];  // -(m_42 - m_32)
	p->nz = -M[11] + M[10]; // -(m_43 - m_33)
	p->d =   M[15] - M[14]; //  (m_44 - m_34) because d in plane is really (-d)
	p->is_norm = 0;
	// It is not neccesary to normailze the planes for frustum calculation
}