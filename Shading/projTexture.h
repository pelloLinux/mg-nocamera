
#ifndef PROJTEXTURE_H
#define PROJTEXTURE_H

#include <stddef.h>
#include <stdio.h>
#include "trfm3D.h"
#include "camera.h"
#include "texture.h"
#include "intersect.h"




typedef struct {

	//texturaren irudiaren kokapena
	char * image;

	// proiektatuko den testura
	Texture * texture;

	//kamara batean proiektorean behar dituen parametro guztiak daude.
	Camera * kamara;

	//transformazio matrizea
	trfm3D * T;

	//shaderrari pasatuko zaion informazioa
	float * Tm;


} pText;

//  proiekzio-testura suntsitu.
void DestroyProjTexture(pText **thePTexture);

void BindGLProjTexture();
void PlaceSceneProjTexture();
void CreateProjTexture(pText *thePTexture, char* image, float Ex, float Ey, float Ez,
						float AtX, float AtY, float AtZ,
						float UpX, float UpY, float UpZ,
						float fovy, float aspectRatio, float near, float far);

#endif