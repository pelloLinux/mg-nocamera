#version 120

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

uniform struct light_t {
  vec4 position;    // Camera space
  vec3 ambient;     // rgb
  vec3 diffuse;     // rgb
  vec3 specular;    // rgb
  vec3 attenuation; // (constant, lineal, quadratic)
  vec3 spotDir;     // Camera space
  float cosCutOff;  // cutOff cosine
  float exponent;
} theLights[4];     // MG_MAX_LIGHTS

uniform struct material_t {
  vec3  ambient;
  vec3  diffuse;
  vec3  specular;
  float alpha;
  float shininess;
} theMaterial;

attribute vec3 v_position; // Model space
attribute vec3 v_normal;   // Model space
attribute vec2 v_texCoord;

varying vec4 f_color;
varying vec2 f_texCoord;

float diffuse_channel(const vec3 n,
					  const vec3 l) {
  return max(0, dot(n,l));
}

float specular_channel(const vec3 n,
					   const vec3 l,
					   const vec3 v,
					   float m) {

  vec3 r = normalize(2*dot(n,l)*n - l);
  return pow(max(0, dot(r,v)), m);
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  ambient += theLights[i].ambient;
  diffuse += theLights[i].diffuse * difFactor;
  specular += theLights[i].specular * specFactor;
}

void point_light(const in int i,
				 const in vec3 lightDirection,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  ambient += theLights[i].ambient;
  diffuse += theLights[i].diffuse * difFactor;
  specular += theLights[i].specular * specFactor;
}

void spot_light(const in int i,
				const in vec3 lightDirection,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float spotDot; // -1 * lightDirection * spotDirection
  float cSpot;

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  spotDot = max(0.0, dot(-1*lightDirection, theLights[i].spotDir));

  if (spotDot > theLights[i].cosCutOff) {
  	cSpot = pow(spotDot, theLights[i].exponent);
  	ambient  += theLights[i].ambient;
  	diffuse  += theLights[i].diffuse * difFactor * cSpot;
  	specular += theLights[i].specular * specFactor * cSpot;
  }
}

void main() {

  vec4 position_camera_space;
  vec3 viewDirection;

  vec3 ambient = vec3(0.0);
  vec3 diffuse = vec3(0.0);
  vec3 specular = vec3(0.0);

  vec3 normal = normalize(vec3(modelToCameraMatrix * vec4(v_normal, 0.0)));

  float spotAngle;
  float cspot;

  position_camera_space = modelToCameraMatrix * vec4(v_position, 1);
  viewDirection = -normalize(vec3(position_camera_space));

  for(int i=0; i < active_lights_n; ++i) {
	if(theLights[i].position.w == 0.0) {
	  // direction light
	  direction_light(i,
	  				  -normalize(vec3(theLights[i].position)), // lightDirection
	  				  viewDirection,
	  				  normal,
	  				  ambient, diffuse, specular);
	} else {
	  if (theLights[i].cosCutOff == 0.0) {
		point_light(i,
					normalize(vec3(theLights[i].position - position_camera_space)),
					viewDirection,
					normal,
					ambient, diffuse, specular);
	  } else {
		spot_light(i,
				   normalize(vec3(theLights[i].position - position_camera_space)),
				   viewDirection,
				   normal,
				   ambient, diffuse, specular);
	  }
	}
  }
  f_color.rgb = ambient * theMaterial.ambient + diffuse * theMaterial.diffuse + specular * theMaterial.specular;
  f_color.a = theMaterial.alpha;
  f_texCoord = v_texCoord;
  gl_Position = modelToClipMatrix * vec4(v_position, 1);
}
