#version 120

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

varying vec4 f_projTexCoord;

uniform sampler2D texture0;
uniform sampler2D projTextura;

void main()
{
	vec4 f_projTexColor;

	if(f_projTexCoord.z > 0)
		f_projTexColor = texture2DProj(projTextura, f_projTexCoord);

	gl_FragColor = f_projTexColor * f_projTexCoord;

}
