#version 120

uniform int active_lights_n; // Number of active lights (< MG_MAX_LIGHT)

struct material_t {
  	vec3  ambient;
  	vec3  diffuse;
  	vec3  specular;
  	float alpha;
  	float shininess;
};

struct light_t {
  	vec4 position;    // Camera space
  	vec3 ambient;     // rgb
  	vec3 diffuse;     // rgb
  	vec3 specular;    // rgb
  	vec3 attenuation; // (constant, lineal, quadratic)
  	vec3 spotDir;     // Camera space
  	float cosCutOff;  // cutOff cosine
  	float exponent;
};

uniform light_t theLights[4];
uniform material_t theMaterial;

uniform sampler2D texture0; // the color texture
uniform sampler2D bumpmap;  // bump map

varying vec2 f_texCoord;
varying vec3 f_viewDirection;     // tangent space
varying vec3 f_lightDirection[4]; // tangent space
varying vec3 f_spotDirection[4];  // tangent space

float diffuse_channel(const vec3 n,
					  const vec3 l) {
  return max(0, dot(n,l));
}

float specular_channel(const vec3 n,
					   const vec3 l,
					   const vec3 v,
					   float m) {

  vec3 r = normalize(2*dot(n,l)*n - l);
  return pow(max(0, dot(r,v)), m);
}

void direction_light(const in int i,
					 const in vec3 lightDirection,
					 const in vec3 viewDirection,
					 const in vec3 normal,
					 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  ambient += theLights[i].ambient;
  diffuse += theLights[i].diffuse * difFactor;
  specular += theLights[i].specular * specFactor;
}

void point_light(const in int i,
				 const in vec3 lightDirection,
				 const in vec3 viewDirection,
				 const in vec3 normal,
				 inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  ambient += theLights[i].ambient;
  diffuse += theLights[i].diffuse * difFactor;
  specular += theLights[i].specular * specFactor;
}

void spot_light(const in int i,
				const in vec3 lightDirection,
				const in vec3 viewDirection,
				const in vec3 normal,
				inout vec3 ambient, inout vec3 diffuse, inout vec3 specular) {

  float spotDot; // -1 * lightDirection * spotDirection
  float cSpot;

  float difFactor;
  float specFactor;

  difFactor = diffuse_channel(normal, lightDirection);
  specFactor = specular_channel(normal, lightDirection, viewDirection, theMaterial.shininess);

  spotDot = max(0.0, dot(-1*lightDirection, f_spotDirection[i]));

  if (spotDot > theLights[i].cosCutOff) {
  	cSpot = pow(spotDot, theLights[i].exponent);
  	ambient  += theLights[i].ambient;
  	diffuse  += theLights[i].diffuse * difFactor * cSpot;
  	specular += theLights[i].specular * specFactor * cSpot;
  }
}

void main() {

  vec4 color;

  vec4 position_camera_space;
  vec3 viewDirection;

  vec3 ambient = vec3(0.0);
  vec3 diffuse = vec3(0.0);
  vec3 specular = vec3(0.0);

  vec4 baseColor = texture2D(texture0, f_texCoord);
  vec3 normal = texture2D(bumpmap, f_texCoord).rgb * 2.0 - 1.0;
  viewDirection = f_viewDirection;

  float spotAngle;
  float cspot;


  for(int i=0; i < active_lights_n; ++i) {
	if(theLights[i].position.w == 0.0) {
	  // direction light
	  direction_light(i,
	  				  -normalize(vec3(theLights[i].position)), // lightDirection
	  				  viewDirection,
	  				  normal,
	  				  ambient, diffuse, specular);
	} else {
	  if (theLights[i].cosCutOff == 0.0) {
		point_light(i,
					normalize(vec3(f_lightDirection[i])),
					viewDirection,
					normal,
					ambient, diffuse, specular);
	  } else {
		spot_light(i,
				   normalize(vec3(f_lightDirection[i])),
				   viewDirection,
				   normal,
				   ambient, diffuse, specular);
	  }
	}
  }

  
  color.rgb = ambient * theMaterial.ambient + diffuse * theMaterial.diffuse + specular * theMaterial.specular;
  color.a = theMaterial.alpha;

  gl_FragColor = color * baseColor;




}