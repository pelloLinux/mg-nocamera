#version 120


attribute vec3 v_position;
attribute vec3 v_normal;
attribute vec2 v_texCoord;

uniform mat4 modelToCameraMatrix;
uniform mat4 cameraToClipMatrix;
uniform mat4 modelToWorldMatrix;
uniform mat4 modelToClipMatrix;

uniform mat4 pTextMat;
varying vec4 f_projTexCoord;

void main()
{
	//erpinaren posizioa kameraren espazioan hartu,
	vec4 v_position_camera = (modelToCameraMatrix * vec4(v_position, 1));
	//testura--proiekzioaren matrizearen bidez biderkatu, eta fragmen shader -ari pasa.
	f_projTexCoord = pTextMat * v_position_camera;
	gl_Position = f_projTexCoord;
}