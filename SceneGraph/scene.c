#include <assert.h>
#include "scene.h"
#include "tools.h"
#include "trfm3D.h"
#include "textureManager.h"
#include "materialManager.h"
#include "lightManager.h"
#include "gObjectManager.h"
#include "cameraManager.h"
#include "avatarManager.h"
#include "shaderManager.h"
#include "node.h"
#include "image.h"
#include "hash.h"
#include "renderState.h"


static Node *rootNode = NULL;

static trfm3D *I;

static RenderState *rstate;

// Functions

void InitScene() {

	I = CreateTrfm3D();
	rstate = CreateRenderState();
	InitCameraManager();
	InitAvatarManager();
	InitGObjectManager();
	InitMaterialManager();
	InitImageManager();
	InitTextureManager();
	InitLightManager();
	InitShaderManager();
    InitNodeManager();
	rootNode = CreateNode("MG_ROOTNODE");
	SceneSetTextureFunction(GL_MODULATE);
}

void DestroyScene() {

	DestroyNode(&rootNode);
	DestroyRenderState(&rstate);
	if (I) DestroyTrfm3D(&I);
    DestroyNodeManager();
	DestroyShaderManager();
	DestroyLightManager();
	DestroyImageManager();
	DestroyTextureManager();
	DestroyMaterialManager();
	DestroyGObjectManager();
	DestroyCameraManager();
	DestroyAvatarManager();
}

// Render state

RenderState *RenderStateScene() {
	return rstate;
}

// Node Management

Node *RootNodeScene() {

	return rootNode;
}


void SetShadingScene(GLenum face, GLenum mode) {

	glPolygonMode(face, mode);
}

void AttachNodeScene(Node *theNode) {

	AttachNode(rootNode, theNode);
}

// TODO: deal with transparent objects

void DrawScene() {

	if (rootNode) {
		DrawNode(rootNode);
	}
}
